import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(
    private authService: AuthService,
    private router: Router,
    private alertController: AlertController
  ) {}

  async logout(){
    const alert = await this.alertController.create({
      header: 'Logout',
      message: 'Lanjutin keluar dari akun ini ?',
      buttons: [
        {text: 'Batal',role: 'false',cssClass: 'secondary',},
        {text: 'Lanjutin',role: 'true',}
      ],
      backdropDismiss: false
    });
    alert.onDidDismiss().then(async (modelData) => {
      if(modelData.role == 'true'){
        try{
          this.authService.removeSession().then((response)=>{
            if(response){
              this.router.navigateByUrl('/login', { replaceUrl: true });
            }
          });
        }catch(err){
          console.log(err);
        }
      }
    });
    await alert.present();
  }

}
