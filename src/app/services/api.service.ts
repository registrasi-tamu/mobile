import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http : HttpClient,
    private router : Router,
    private authService : AuthService,
    private alertController : AlertController
  ) { }

  methodGet(ctrl: string, token: string, prefix:string = ''): Observable<any> {
    const isHead = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Accept':'application/json', 'Authorization': `Bearer ${token}` }) };
    return this.http.get(`${environment.baseUrl}${ctrl}`, isHead)
      .pipe(
        map(results => {
          return results;
        }),
        catchError(async (error) => {
          if(error.status ==401){
            const alert = await this.alertController.create({
              header: 'Hi, Admin',
              message: 'Sesi anda habis, silahkan login kembali !',
              buttons: [
                {text: 'YA',role: 'true',}
              ],
              backdropDismiss: false
            });
            alert.present();
            this.authService.removeSession().then((response)=>{
              if(response){
                this.router.navigateByUrl('/login', {replaceUrl:true});
              }
            });
          }else{
            const alert = await this.alertController.create({
              header: 'Hi, Admin',
              message: `terjadi kesalahan sistem `+error.message,
              buttons: [
                {text: 'YA',role: 'true',}
              ],
              backdropDismiss: false
            });
            alert.present();
          }
        })
      );
  }

  methodDelete(ctrl: string, token: string, prefix:string = ''): Observable<any> {
    const isHead = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Accept':'application/json', 'Authorization': `Bearer ${token}` }) };
    return this.http.delete(`${environment.baseUrl}${ctrl}`, isHead)
      .pipe(
        map(results => {
          return results;
        }),
        catchError(async (error) => {
          if(error.status ==401){
            const alert = await this.alertController.create({
              header: 'Hi, Admin',
              message: 'Sesi anda habis, silahkan login kembali !',
              buttons: [
                {text: 'YA',role: 'true',}
              ],
              backdropDismiss: false
            });
            alert.present();
            this.authService.removeSession().then((response)=>{
              if(response){
                this.router.navigateByUrl('/login', {replaceUrl:true});
              }
            });
          }else{
            const alert = await this.alertController.create({
              header: 'Hi, Admin',
              message: `terjadi kesalahan sistem `+error.message,
              buttons: [
                {text: 'YA',role: 'true',}
              ],
              backdropDismiss: false
            });
            alert.present();
          }
        })
      );
  }

  methodPost(ctrl: string, bodyObj: any, token: string, prefix:string = ''): Observable<any> {
    const isHead = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Accept':'application/json', 'Authorization': `Bearer ${token}` }) };
    return this.http.post(`${environment.baseUrl}${ctrl}`, bodyObj, isHead)
      .pipe(
        map(results => {
          return results;
        }),
        catchError(async (error) => {
          if(error.status ==401){
            const alert = await this.alertController.create({
              header: 'Hi, Admin',
              message: 'Sesi anda habis, silahkan login kembali !',
              buttons: [
                {text: 'YA',role: 'true',}
              ],
              backdropDismiss: false
            });
            alert.present();
            this.authService.removeSession().then((response)=>{
              if(response){
                this.router.navigateByUrl('/login', {replaceUrl:true});
              }
            });
          }else{
            const alert = await this.alertController.create({
              header: 'Hi, Admin',
              message: `terjadi kesalahan sistem `+error.message,
              buttons: [
                {text: 'YA',role: 'true',}
              ],
              backdropDismiss: false
            });
            alert.present();
          }
        })
      );
  }
  methodPut(ctrl: string, bodyObj: any, token: string, prefix:string = ''): Observable<any> {
    const isHead = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Accept':'application/json', 'Authorization': `Bearer ${token}` }) };
    return this.http.put(`${environment.baseUrl}${ctrl}`, bodyObj, isHead)
      .pipe(
        map(results => {
          return results;
        }),
        catchError(async (error) => {
          if(error.status ==401){
            const alert = await this.alertController.create({
              header: 'Hi, Admin',
              message: 'Sesi anda habis, silahkan login kembali !',
              buttons: [
                {text: 'YA',role: 'true',}
              ],
              backdropDismiss: false
            });
            alert.present();
            this.authService.removeSession().then((response)=>{
              if(response){
                this.router.navigateByUrl('/login', {replaceUrl:true});
              }
            });
          }else{
            const alert = await this.alertController.create({
              header: 'Hi, Admin',
              message: `terjadi kesalahan sistem `+error.message,
              buttons: [
                {text: 'YA',role: 'true',}
              ],
              backdropDismiss: false
            });
            alert.present();
          }
        })
      );
  }
}
