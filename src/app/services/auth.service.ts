import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Storage } from '@ionic/storage-angular';
// import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private storage: Storage
  ) { }

  async setSession(val: any){
    await this.storage.create();
    await this.storage.set(environment.sessionKey, val);
    return 1;
  }

  async getSession(){
    await this.storage.create();
    let dataSession = await this.storage.get(environment.sessionKey);
    return dataSession;
  }

  async setStatusDriver(val: any){
    await this.storage.create();
    await this.storage.set(environment.sessionKey, val);
    return 1;
  }

  async removeSession(){
    await this.storage.create();
    this.storage.remove(environment.sessionKey);
    return 1;
  }
}
