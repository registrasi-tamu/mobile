import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AlertController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service'; 

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  showPassword: boolean = false;
  username: string = '';
  password: string = '';

  constructor(
    private router: Router,
    private apiService : ApiService,
    private authService : AuthService,
    private alertController : AlertController,
  ) { }

  ngOnInit() { 
    try{
      this.authService.getSession().then((response)=>{
        if(response){
          this.router.navigateByUrl('/tabs/tab1', { replaceUrl: true });
        }
      });
    }catch(err){
      console.log(err);
      alert(err.message);
    }
  }
  async showHidePassword(){
    this.showPassword = (this.showPassword == true ? false : true);
  }
  async login(){ 
    try{
      if(this.username && this.password){
        let form = JSON.stringify({
          'username': this.username,
          'password' : this.password,
        });
        this.apiService.methodPost('login',form,'').subscribe( 
          async (response)=>{
            if(response.status == 200){
              this.authService.setSession(response.data).then((res)=>{
                if(res){
                  this.router.navigateByUrl('/tabs/tab1', { replaceUrl: true });
                }
              });
            }else{
              const alert = await this.alertController.create({
                header: 'Hi, Admin',
                message: 'Username atau password tidak cocok !',
                buttons: [
                  {text: 'YA',role: 'true',}
                ],
                backdropDismiss: false
              });
              alert.present();
            }
          }
        ); 
      }else{
        const alert = await this.alertController.create({
          header: 'Hi, Admin',
          message: 'Lengkapi data !',
          buttons: [
            {text: 'YA',role: 'true',}
          ],
          backdropDismiss: false
        });
        alert.present();
      }
    }catch(err){
      console.log(err);
      alert(err.message);
    }

  }

}
