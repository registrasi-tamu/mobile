import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { AlertController, ModalController } from '@ionic/angular';
import * as moment from 'moment';
import { SearchPage } from 'src/app/components/search/search.page';

@Component({
  selector: 'app-riwayat',
  templateUrl: './riwayat.page.html',
  styleUrls: ['./riwayat.page.scss'],
})
export class RiwayatPage implements OnInit {

  isAuth : any = [];
  isData : any = [];
  
  skeletonShow : number = 1;
  offset : number = 0;
  limit : number = 10;
  isDataTotal : number = 0; 
  
  searchKeyword : any;
  constructor(
    private authService : AuthService,
    private apiService : ApiService,
    private router : Router,
    private alertController: AlertController,
    private modalController: ModalController,
  ) { }

  async ngOnInit() {
    this.authService.getSession().then(async (res)=>{
      this.isAuth = res;
      // await this.doRefresh();
    })
  }

  async ionViewDidEnter(){
    await this.doRefresh();
  }

  async getData(event = null){ 
    try{
      this.apiService.methodGet(`guest-book/0/?keyword=${this.searchKeyword}&status=OUT`, this.isAuth['token']).subscribe(async(response:any)=>{
        if(response.status == 200){
          response.data.forEach((val:any, index:number) => {
            response.data[index].created = moment(val.createdAt).format('YYYY-MM-DD HH:mm:ss');
            response.data[index].updated = moment(val.updatedAt).format('YYYY-MM-DD HH:mm:ss');
            this.isData.push(response.data[index]);
          });
          this.offset += this.limit; 
          this.isDataTotal = response.total;
        }
        this.skeletonShow = 0;
      });
      if(event){
        setTimeout(() => {
          event.target.complete();
        }, 500);
      }
    }catch(error){
      console.log(error);
    }
  }

  async searchData(){
    let modal = await this.modalController.create({
      component : SearchPage,
      showBackdrop: false,
      cssClass: 'custom-modal-css',
      swipeToClose: false,
      componentProps: {
        'type' : 1, 
        'keyword' : this.searchKeyword,
      }
    });
    modal.onDidDismiss().then(async (modelData) => {
      if(modelData.role == "true"){
        
        this.searchKeyword = modelData.data.keyword;

        this.skeletonShow = 1;
        this.offset = 0;
        this.isDataTotal = 0;
        this.isData = [];
        this.getData();      
      }
    });
    return await modal.present();
  }

  async doDelete(index:any){

    const alert = await this.alertController.create({
      header: 'Hapus Data',
      message: 'Lanjutin hapus data ini ?',
      buttons: [
        {text: 'Batal',role: 'false',cssClass: 'secondary',},
        {text: 'Lanjutin',role: 'true',}
      ],
      backdropDismiss: false
    });
    alert.onDidDismiss().then(async (modelData) => {
      if(modelData.role == 'true'){
        try{
          this.apiService.methodDelete(`guest-book/${this.isData[index].id}`, this.isAuth.token).subscribe((response:any)=>{
            this.doRefresh();
          });
        }catch(err){
          console.log(err);
        }
      }
    });
    await alert.present();
  }

  doRefresh(event = null){
    if(event){
      setTimeout(() => {
        event.target.complete();
      }, 500);
    }    
    this.skeletonShow = 1;
    this.offset = 0;
    this.isDataTotal = 0;
    this.isData = [];
    this.searchKeyword = "";
    this.getData();
  }
}
