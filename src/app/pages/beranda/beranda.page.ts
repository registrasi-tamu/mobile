import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { AlertController, ModalController } from '@ionic/angular';
import * as moment from 'moment';
import { SearchPage } from 'src/app/components/search/search.page';
import { GuestPage } from '../guest/guest.page';

@Component({
  selector: 'app-beranda',
  templateUrl: './beranda.page.html',
  styleUrls: ['./beranda.page.scss'],
})
export class BerandaPage implements OnInit {

  isAuth : any = [];
  isData : any = [];
  isDataOut : any = [];
  searchKeyword : any = '';
  constructor(
    private authService : AuthService,
    private apiService : ApiService,
    private router : Router,
    private alertController: AlertController,
    private modalController: ModalController,
  ) { }

  async ngOnInit() {
    this.authService.getSession().then(async (res)=>{
      this.isAuth = res;
      await this.getData();
    })
  } 

  async ionViewDidEnter(){
    this.ngOnInit();
  }

  async getData(){ 
    moment.locale('id');
    try{
      this.apiService.methodGet(`guest-book/0/?keyword=${this.searchKeyword}&status=IN`, this.isAuth['token']).subscribe(async(response:any)=>{
        if(response.status == 200){
          response.data.forEach((val:any, index:number) => {
            response.data[index].created = moment(val.createdAt).format('YYYY-MM-DD HH:mm:ss');
          });
          this.isData = response.data;
        }
      });
      this.apiService.methodGet(`guest-book/0/?keyword=&status=OUT`, this.isAuth['token']).subscribe(async(response:any)=>{
        if(response.status == 200){
          this.isDataOut = response.data;
        }
      });
    }catch(error){
      console.log(error);
    }
  }

  async searchData(){
    let modal = await this.modalController.create({
      component : SearchPage,
      showBackdrop: false,
      cssClass: 'custom-modal-css',
      swipeToClose: false,
      componentProps: {
        'type' : 1, 
        'keyword' : this.searchKeyword,
      }
    });
    modal.onDidDismiss().then(async (modelData) => {
      if(modelData.role == "true"){
        this.searchKeyword = modelData.data.keyword;
        this.getData();      
      }
    });
    return await modal.present();
  }
  async addData(idx:any = '', from:string){
    try{
      let modal = await this.modalController.create({
        component : GuestPage,
        showBackdrop: false,
        cssClass: 'custom-modal-css',
        swipeToClose: false,
        componentProps: {
          'id' : (from =='update' ? this.isData[idx].id : ''),
          'id_card_number' : (from =='update' ? this.isData[idx].id_card_number : ''),
          'card_type' : (from =='update' ? this.isData[idx].card_type : ''),
          'name' : (from =='update' ? this.isData[idx].name : ''),
          'address' : (from =='update' ? this.isData[idx].address : ''),
          'phone_number' : (from =='update' ? this.isData[idx].phone_number : ''),
          'necessary' : (from =='update' ? this.isData[idx].necessary : ''),
          'card_number' : (from =='update' ? this.isData[idx].card_number : ''),
          'status' : (from =='update' ? this.isData[idx].status : ''),
        }
      });
      modal.onDidDismiss().then(async (modelData) => {
        if(modelData.role == "true"){
          let url = '';
          if(modelData.data.id){
            this.apiService.methodPut(`guest-book/${modelData.data.id}`,JSON.stringify(modelData.data), this.isAuth.token).subscribe(async (response:any)=>{
              if(response.status == 200){
                this.getData();
              }else{
                const alert = await this.alertController.create({
                header: 'Hi, Admin',
                message: 'Gagal Menyimpan Data !',
                buttons: [
                  {text: 'YA',role: 'true',}
                ],
                backdropDismiss: false
                });
                alert.present();
              }
            })
          }else{
            this.apiService.methodPost(`guest-book`,JSON.stringify(modelData.data), this.isAuth.token).subscribe(async (response:any)=>{
              if(response.status == 200){
                this.getData();
              }else{
                const alert = await this.alertController.create({
          header: 'Hi, Admin',
          message: 'Gagal Menyimpan Data !',
          buttons: [
            {text: 'YA',role: 'true',}
          ],
          backdropDismiss: false
        });
        alert.present();
              }
            })
          }
          this.getData();      
        }
      });
      return await modal.present();
    }catch(err){
      console.log(err);
    }
  }



  doRefresh(event = null){
    if(event){
      setTimeout(() => {
        event.target.complete();
      }, 500);
    }
    this.searchKeyword = '';
    this.ngOnInit()
  }
}
