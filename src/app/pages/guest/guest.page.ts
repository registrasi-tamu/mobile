import { Component, OnInit, Input } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-guest',
  templateUrl: './guest.page.html',
  styleUrls: ['./guest.page.scss'],
})
export class GuestPage implements OnInit {

  @Input() id: any = '';
  @Input() id_card_number: any = '';
  @Input() card_type : any = '';
  @Input() name : any = '';
  @Input() address : any = '';
  @Input() phone_number : any = '';
  @Input() necessary : any = '';
  @Input() card_number : any = '';
  @Input() status : any = '';

  constructor(
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit() {
  }

  async Save() {
    if(this.id_card_number != '' && this.card_type != '' && this.name != '' && this.address != '' && this.phone_number != '' && this.necessary != '' && this.status != '' && this.card_number != ''){
      this.modalController.dismiss({
        id : this.id,
        id_card_number : this.id_card_number,
        card_type : this.card_type,
        name : this.name,
        address : this.address,
        phone_number : this.phone_number,
        necessary : this.necessary,
        card_number : this.card_number,
        status : this.status
      }, 'true');
    }else{
      const alert = await this.alertController.create({
        header: 'Hi, Admin',
        message: 'Lengkapi Semua Data !',
        buttons: [
          {text: 'YA',role: 'true',}
        ],
        backdropDismiss: false
        });
        alert.present();
    }
  }

  Close() {
    this.modalController.dismiss(
      {
        id : "",
        card_type : "",
        id_card_number : "",
        name : "",
        address : "",
        phone_number : "",
        necessary : "",
        card_number : "",
        status : ""
      }, 'false'
    );
  }

}
