import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  @Input() type: number = 0;
  @Input() keyword : any = '';
  @Input() dateStart : any = '';
  @Input() dateEnd : any = '';

  constructor(
    private modalController: ModalController,
  ) { }

  ngOnInit() {
  }

  LoadBackPage() {
    this.modalController.dismiss({
      keyword: this.keyword
    }, 'true');
  }

  CloseFilter() {
    this.modalController.dismiss(
      {
        keyword: "",
        dateStart: "",
      }, 'false'
    );
  }

}
