# MOBILE REGISTRASI TAMU

This project was generated with nodeJs IONIC 6 , ANGULAR 14 AND CORDOVA 11

# Getting Started

Before you setup the application, make sure you have the prerequisites:
- [ANGULAR](https://angular.io/) 14
- [IONIC](https://ionicframework.com/)


### Install Dependencies

```bash
$ npm install
```

### Running the application
```bash
ionic serve
```

### Build the application dev
```bash
ionic cordova prepare android
ionic cordova build android
```
